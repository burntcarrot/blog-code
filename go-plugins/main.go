package main

import (
	"errors"
	"fmt"
	"log"
	"plugin"

	"github.com/spf13/cobra"
)

func main() {
	mainCmd := &cobra.Command{
		Use:   "main",
		Short: "Simple cobra app",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("Main app called!")
		},
	}

	// Load the plugin.
	pluginCmd, err := LoadPlugin("./plugin/plugin.so", "PluginCmd")
	if err != nil {
		log.Fatalf("failed to load plugin: %v\n", err)
	}

	// Register the plugin at runtime.
	mainCmd.AddCommand(pluginCmd)

	if err := mainCmd.Execute(); err != nil {
		log.Fatalf("error: %v\n", err)
	}
}

// LoadPlugin loads a cobra command from a shared object file.
func LoadPlugin(pluginPath, cmdName string) (*cobra.Command, error) {
	p, err := plugin.Open(pluginPath)
	if err != nil {
		return nil, err
	}

	c, err := p.Lookup("New" + cmdName)
	if err != nil {
		return nil, err
	}

	// Get access to the plugin function and get the command.
	pluginFunc, ok := c.(func() *cobra.Command)
	if !ok {
		return nil, errors.New("failed to perform a lookup.")
	}

	pluginCmd := pluginFunc()

	return pluginCmd, nil
}
