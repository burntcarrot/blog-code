package main

import (
	"fmt"

	"github.com/spf13/cobra"
)

func NewPluginCmd() *cobra.Command {
	pluginCmd := &cobra.Command{
		Use:   "plugin",
		Short: "Plugin command",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("Plugin called!")
		},
	}

	return pluginCmd
}
